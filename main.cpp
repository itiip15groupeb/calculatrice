/*
 * main.cpp
 */
#include <iostream>
#include <string>
#include <libsx.h>
#include "Callback.hpp"
#include "CurrentValue.hpp"

void init_display (int argc, char** argv, void* currentValue) {
	
	//// WIDGETS CREATION ////

	//display number
	Widget stringEntry1 = MakeStringEntry((char*)"", 134, NULL, NULL);
	
	//Buttons number
	Widget button0 = MakeButton((char*)"0", zero, currentValue);
	Widget button1 = MakeButton((char*)"1", one, currentValue);
	Widget button2 = MakeButton((char*)"2", two, currentValue);
	Widget button3 = MakeButton((char*)"3", three, currentValue);
	Widget button4 = MakeButton((char*)"4", four, currentValue);
	Widget button5 = MakeButton((char*)"5", five, currentValue);
	Widget button6 = MakeButton((char*)"6", six, currentValue);
	Widget button7 = MakeButton((char*)"7", seven, currentValue);
	Widget button8 = MakeButton((char*)"8", eight, currentValue);
	Widget button9 = MakeButton((char*)"9", nine, currentValue);
	
	//Buttons operands
	Widget buttonDot = MakeButton((char*)".", dot, currentValue);
	Widget buttonPerCent = MakeButton((char*)"%", perCent, currentValue);
	Widget buttonSlash = MakeButton((char*)"/", slash, currentValue);
	Widget buttonCross = MakeButton((char*)"x", cross, currentValue);
	Widget buttonMinus = MakeButton((char*)"-", minus, currentValue);
	Widget buttonPlus = MakeButton((char*)"+", plus, currentValue);
	Widget buttonCarry = MakeButton((char*)"^2", carry, currentValue);

	//Buttons system
	Widget buttonD1 = MakeButton((char*)"Dl", D1, currentValue);
	Widget buttonC1 = MakeButton((char*)"Cl", C1, currentValue);
	Widget buttonEnter = MakeButton((char*)" = ", enter, currentValue);
	Widget buttonMPlus = MakeButton((char*)"M+", MPlus, currentValue);
	Widget buttonMR = MakeButton((char*)"MR", MR, currentValue);
	Widget buttonMC = MakeButton((char*)"MC", MC, currentValue);
	Widget buttonSpace = MakeButton((char*)"_", space, currentValue);


	//// WIDGETS POSITION ////

	//line 1
	SetWidgetPos(button7, 10, stringEntry1, PLACE_UNDER, stringEntry1);
	//line 2
	SetWidgetPos(button8, PLACE_RIGHT, button7, PLACE_UNDER, stringEntry1);
	SetWidgetPos(button9, PLACE_RIGHT, button8, PLACE_UNDER, stringEntry1);
	SetWidgetPos(button9, PLACE_RIGHT, button8, PLACE_UNDER, stringEntry1);
	SetWidgetPos(buttonSlash, PLACE_RIGHT, button9, PLACE_UNDER, stringEntry1);
	SetWidgetPos(buttonD1, PLACE_RIGHT, buttonSlash, PLACE_UNDER, stringEntry1);
	SetWidgetPos(buttonMPlus, PLACE_RIGHT, buttonD1, PLACE_UNDER, stringEntry1);
	//line 3
	SetWidgetPos(button4, 10, button7, PLACE_UNDER, button7);
	SetWidgetPos(button5, PLACE_RIGHT, button4, PLACE_UNDER, button7);
	SetWidgetPos(button6, PLACE_RIGHT, button5, PLACE_UNDER, button7);
	SetWidgetPos(buttonCross, PLACE_RIGHT, button6, PLACE_UNDER, button7);
	SetWidgetPos(buttonC1, PLACE_RIGHT, buttonCross, PLACE_UNDER, button7);
	SetWidgetPos(buttonMR, PLACE_RIGHT, buttonC1, PLACE_UNDER, button7);
	//line 4
	SetWidgetPos(button1, 10, button4, PLACE_UNDER, button4);
	SetWidgetPos(button2, PLACE_RIGHT, button1, PLACE_UNDER, button4);
	SetWidgetPos(button3, PLACE_RIGHT, button2, PLACE_UNDER, button4);
	SetWidgetPos(buttonMinus, PLACE_RIGHT, button3, PLACE_UNDER, button4);
	SetWidgetPos(buttonCarry, PLACE_RIGHT, buttonMinus, PLACE_UNDER, button4);
	SetWidgetPos(buttonMC, PLACE_RIGHT, buttonCarry, PLACE_UNDER, button4);
	//line 5
	SetWidgetPos(button0, 10, button1, PLACE_UNDER, button1);
	SetWidgetPos(buttonDot, PLACE_RIGHT, button0, PLACE_UNDER, button1);
	SetWidgetPos(buttonPerCent, PLACE_RIGHT, buttonDot, PLACE_UNDER, button1);
	SetWidgetPos(buttonPlus, PLACE_RIGHT, buttonPerCent, PLACE_UNDER, button1);
	SetWidgetPos(buttonSpace, PLACE_RIGHT, buttonPlus, PLACE_UNDER, button1);
	SetWidgetPos(buttonEnter, PLACE_RIGHT, buttonSpace, PLACE_UNDER, button1);

	// Display field
	CurrentValue* pCurrentValue = static_cast<CurrentValue*>(currentValue);
	pCurrentValue->zoneAffichage = stringEntry1;
	// Color of system
	GetStandardColors();
	// Allow to display window
	ShowDisplay();
}

int main(int argc, char** argv) 
{
	
	CurrentValue currentValue;
	if (OpenDisplay(argc, argv) == 0) {
		std::cerr << "Error: Graphical interface can't be displayed." << std::endl;
	return 1;
	}
	
	init_display(argc, argv, &currentValue);
	MainLoop();
	return 0;
}


