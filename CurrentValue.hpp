/*
 * CurrentValue.hpp
 */
#include <iostream>
#include <libsx.h>
 
struct CurrentValue{ 
	CurrentValue();
	CurrentValue(double valeur, Widget zoneAffichage);
		
	double valeur;
	double memoire;
	Widget zoneAffichage;
};
