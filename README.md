# Groupe B #
*Jérémy MAISSE, Jordan HABRAN, Mehdi BENRALATE*

Projet de POO en C++ de Mr Abou, création d'une calculatrice HP, qui utilise la librairie libsx.

### Récupérer le code ###

* Cloner ce répertoire à partir de son [URL](https://bitbucket.org/itiip15groupeb/calculatrice) dans le répertoire de votre choix sur votre ordinateur :
	
```
cd $HOME/Documents
git clone https://nevermindLtd@bitbucket.org/itiip15groupeb/calculatrice.git
```
* Vous devez avoir la libraire libsx. Pour instaler cette libraire: ```sudo apt-get install libsx-dev libsx0```

* Compiler le projet: ```make```

* Exécuter le programme: ```make run```

* Néttoyer le projet:
    * effacer les .o: ```make clean```
    * effacer les .o et executable: ```make cleaner```


### Librairie libsx ###

Une fois téléchargée,la documentation de la librairie se trouve ici: 
[file:///usr/share/doc/libsx-dev/html/libsx.html](file:///usr/share/doc/libsx-dev/html/libsx.html)