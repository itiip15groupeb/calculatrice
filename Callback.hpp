/*
 * Callback.hpp
 */
#include <iostream>
#include <libsx.h>

void zero(Widget button, void* d);
void one(Widget button, void* d);
void two(Widget button, void* d);
void three(Widget button, void* d);
void four(Widget button, void* d);
void five(Widget button, void* d);
void six(Widget button, void* d);
void seven(Widget button, void* d);
void eight(Widget button, void* d);
void nine(Widget button, void* d);

void dot(Widget button, void* d);
void perCent(Widget button, void* d);
void slash(Widget button, void* d);
void cross(Widget button, void* d);
void minus(Widget button, void* d);
void plus(Widget button, void* d);
void carry(Widget button, void* d);


void D1(Widget button, void* d);
void C1(Widget button, void* d);
void enter(Widget button, void* d);
void MPlus(Widget button, void* d);
void MR(Widget button, void* d);
void MC(Widget button, void* d);
void space(Widget button, void* d);
