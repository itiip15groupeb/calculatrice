/*
 * Callback.cpp
 */
#include <cstring>
#include "Callback.hpp"
#include "CurrentValue.hpp"
#include <sstream>
#include <vector>
#include <math.h> 

const char operators[]="+-/*%^\0";
const char separators[]="+-/*%^ \0";

//////////// OUTILS ////////////

/*
 * Converti un double en string
 */
std::string doubleToString(double & number){
	std::ostringstream strs;
	strs << number;
	std::string str = strs.str();
	return str;
}
/*
 * Converti un string en char*
 */
char* stringToChar(std::string str){
	return strdup(str.c_str());
}
/*
 * Converti un double en char*
 */
char* doubleToChar(double & number){
	return stringToChar(doubleToString(number));
}
/*
 * Converti un char* en double
 */
double charToDouble(char* chaine){
	//return *reinterpret_cast<double*>(chaine);
	return atof(chaine);
}
bool isOperator(char c){
	std::string sOperators=operators;
	for(int i=0;i<sOperators.length();i++){
		if(c==sOperators[i])
			return true;
	}
	return false;
}
/*
 * Parse dans un vecteur les nombres de l'opération
 */
std::vector<double> parseNumber(char* operation) {
    char* cNumber;
	std::vector<double> vectorNumbers;
    
    // lire la première cellule
	cNumber = strtok(operation, separators);
	vectorNumbers.push_back(charToDouble(cNumber));
    
	while (cNumber) {
        // lire les cellules restantes
		cNumber = strtok(0,separators);
		if (cNumber){ // évite de pousser des chaînes vides
			//vectorNumbers.push_back(charToDouble(cNumber));
  			vectorNumbers.insert(vectorNumbers.begin(),  charToDouble(cNumber));
		}
    }
    return vectorNumbers;
}
/*
 * Parse dans un vecteur les opérateurs de l'opération
 */
std::vector<char> parseOperators(char* operation) {
    //char cOperator;
	std::vector<char> vectorOperators;
	std::string currentString=operation;

	for (int i=0;i<currentString.length(); i++)
	{// Pour chaque caractère du texte
		if(isOperator(currentString[i]))
		{// Si c'est un opérateurs
			vectorOperators.push_back(currentString[i]);
		}
	}
    return vectorOperators;
}

//////////// CALLBACK ////////////

/*
 * Callback button0
 * Rôle: rajoute 0 à la suite du texte affiché
 */
void zero(Widget button, void* d) {
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "0");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button1
 * Rôle: rajoute 1 à la suite du texte affiché
 */
void one(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "1");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button2
 * Rôle: rajoute 2 à la suite du texte affiché
 */
void two(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "2");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button3
 * Rôle: rajoute 3 à la suite du texte affiché
 */
void three(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "3");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button4
 * Rôle: rajoute 4 à la suite du texte affiché
 */
void four(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "4");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button5
 * Rôle: rajoute 5 à la suite du texte affiché
 */
void five(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "5");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button6
 * Rôle: rajoute 6 à la suite du texte affiché
 */
void six(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "6");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button7
 * Rôle: rajoute 7 à la suite du texte affiché
 */
void seven(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "7");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button8
 * Rôle: rajoute 8 à la suite du texte affiché
 */
void eight(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "8");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback button9
 * Rôle: rajoute 9 à la suite du texte affiché
 */
void nine(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "9");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonDot
 * Rôle: rajoute un point à la suite du texte affiché
 */
void dot(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, ".");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonPerCent
 * Rôle: ???
 */
void perCent(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "%");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonSlash
 * Rôle: rajoute un diviser à la suite du text affiché
 */
void slash(Widget button, void* d){	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "/");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonCross
 * Rôle: rajoute un multiplier à la suite du text affiché
 */
void cross(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "*");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonMinus
 * Rôle: rajoute un moin à la suite du text affiché
 */
void minus(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "-");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonPlus
 * Rôle: rajoute un plus à la suite du text affiché
 */
void plus(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "+");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonCarry
 * Rôle: rajoute un carré à la suite du text affiché
 */
void carry(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, "^");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonD1
 * Rôle: efface le dernier caractère tapé
 */
void D1(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	if (texte.length()>1){
		//enlèvement du dernier caractère
		char resultText[texte.length()-1]="";
		for(int i=0; i<texte.length()-1;i++)
		{
			const char recupCharI[1] ={ recupTexte[i] };
			std::strcat(resultText, recupCharI);
		}
		// affichage
		 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
	}
	else
	{
		// affichage
		 SetStringEntry(pCurrentValue->zoneAffichage, (char*)"");
	}
}
/*
 * Callback buttonC1
 * Rôle: efface tout le texte affiché
 */
void C1(Widget button, void* d){
	// supression du texte
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	pCurrentValue->valeur=0;
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, (char*)(""));
}
/*
 * Callback buttonMPlus
 * Rôle: ajoute la somme affiché à la mémoire
 */
void MPlus(Widget button, void* d){
	//récupération du texte
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	char* currentText = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string currentString=currentText;
	//vérification du texte
	for (std::string::iterator it=currentString.begin(); it!=currentString.end(); ++it)
	{// Pour chaque caractère du texte
		if (isalpha(*it)) 
		{// Si ce n'est pas un chiffre
			return;
		}
	}
	//Ici, on est sur que le texte ne contiens qu'un nombre (pas d'alphanumerique ni opérateur)
	pCurrentValue->memoire+= charToDouble(GetStringEntry(pCurrentValue->zoneAffichage));

	SetStringEntry(pCurrentValue->zoneAffichage, (char*)("")); //Comme ça Mehdi il est content
}
/*
 * Callback buttonMR
 * Rôle: permet d'afficher la valeur de la mémoire
 */
void MR(Widget button, void* d){

	//récupération de la mémoire
	double recupMemoire;
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupMemoire = pCurrentValue->memoire;
	std::string stringMemoire = doubleToString(recupMemoire);

	//récupération du texte
	char* currentText = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string stringText=currentText;

	// ajout de la mémoire au texte courant
	char resultText[stringMemoire.length()+stringText.length()]="";
	std::strcat(resultText, stringToChar(stringText));
	std::strcat(resultText, stringToChar(stringMemoire));//Comme ça Mehdi il est content

	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonMC
 * Rôle: permet d’effacer la mémoire
 */
void MC(Widget button, void* d){
	((CurrentValue*)d)->memoire=0.0;
}
/*
 * Callback buttonSpace
 * Rôle: d'ajouter un espace à la suite du texte affiché
 */
void space(Widget button, void* d){
	//récupération de l'entrée
	char* recupTexte = (char*)"";
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	recupTexte = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string texte = recupTexte;
	//rajout du nombre
	char resultText[texte.length()+1]="";
	std::strcat(resultText, recupTexte);
	std::strcat(resultText, " ");
	
	// affichage
	 SetStringEntry(pCurrentValue->zoneAffichage, resultText);
}
/*
 * Callback buttonEnter
 * Rôle: resoud le calcul du text affiché
 */
void enter(Widget button, void* d){
	//récupération du texte
	CurrentValue* pCurrentValue = (CurrentValue*)d;
	char* currentText = GetStringEntry(pCurrentValue->zoneAffichage);
	std::string currentString=currentText;
	if(currentString=="")return; // si texte vide
	//vérification du texte
	for (std::string::iterator it=currentString.begin(); it!=currentString.end(); ++it)
	{// Pour chaque caractère du texte
		if (isalpha(*it)) 
		{// Si ce n'est pas un chiffre
			if(currentString.find(operators)==std::string::npos)
			{// Si ce n'est pas un opérateurs
				SetStringEntry(pCurrentValue->zoneAffichage,(char*)"ERROR: alphanumeric");
				return ;
			}			
		}
	}
	//stokage des opérateurs et opérandes
	std::vector<double> vectorNumbers=parseNumber(currentText);
	std::vector<char> vectorOperators=parseOperators(stringToChar(currentString));
	
	if(!(vectorOperators.size()+1==vectorNumbers.size())){
		SetStringEntry(pCurrentValue->zoneAffichage,(char*)"ERROR: incorrect operation");
		return;
	}

	//calcul
	while(vectorNumbers.size()>1){
		switch(vectorOperators[0])
		{
			case '+':
				vectorNumbers[0]=vectorNumbers[0]+vectorNumbers[1];
				vectorNumbers.erase(vectorNumbers.begin()+1);
				vectorOperators.erase(vectorOperators.begin()+0);
				break;
			case '-':
				vectorNumbers[0]=vectorNumbers[0]-vectorNumbers[1];
				vectorNumbers.erase(vectorNumbers.begin()+1);
				vectorOperators.erase(vectorOperators.begin()+0);
				break;
			case '/':
				vectorNumbers[0]=vectorNumbers[0]/vectorNumbers[1];
				vectorNumbers.erase(vectorNumbers.begin()+1);
				vectorOperators.erase(vectorOperators.begin()+0);
				break;
			case '*':
				vectorNumbers[0]=vectorNumbers[0]*vectorNumbers[1];
				vectorNumbers.erase(vectorNumbers.begin()+1);
				vectorOperators.erase(vectorOperators.begin()+0);
				break;
			case '%':
				vectorNumbers[0]=(int)(vectorNumbers[0])%(int)(vectorNumbers[1]);
				vectorNumbers.erase(vectorNumbers.begin()+1);
				vectorOperators.erase(vectorOperators.begin()+0);
				break;
			case '^':
				vectorNumbers[0]=pow(vectorNumbers[0],vectorNumbers[1]);
				vectorNumbers.erase(vectorNumbers.begin()+1);
				vectorOperators.erase(vectorOperators.begin()+0);
				break;
			default:
				SetStringEntry(pCurrentValue->zoneAffichage,(char*)"ERROR: unknown operator");
				return;
		}
	}
	SetStringEntry(pCurrentValue->zoneAffichage,doubleToChar(vectorNumbers[0]));
}